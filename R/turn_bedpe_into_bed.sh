#!/bin/bash
# Whoeps, 19th Jul 2021
# Turn bedpe files into bed, which is easier to process for me. 

bedpedir=$1
outdir=$2

mkdir -p ${outdir}


gawk -v outdir=${outdir} '{FS=OFS="\t"} \
    ((match(FILENAME,/\/([A-Z0-9]*)_[0-9]/, a)) && (!/^#/)) \
    {out = outdir"/"a[1]"_simple.bed"; print "chr"$1,$2,$3,a[1] > out}' \
    ${bedpedir}*.bedpe